<?php
if (!isset($_SESSION)) {
  session_start();
}
/* 
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
require_once('dbaccess.php');

class general {
    var $db ;
	
	//var $auth;
    function general() {
        $this->db = new dbaccess();
		
    }

    function checkLogin(){
    //include'bgtasks.php';
    
    if(!@$_SESSION['userId']){
    //auth::logout(2);
    $_SESSION["userId"] = NULL;
$_SESSION["userEmail"] = NULL;
$_SESSION['userFullname'] = NULL;
unset($_SESSION['userId']);
unset($_SESSION['userEmail']);
unset($_SESSION['userFullname']);
$_SESSION['ewSessionMessage'] = "<div class='errormessage'><b>Session Timeout</b><br>You dont have access to this area or Your session time out after 30 minutes of inactivity. Please you  will need to log in again.</div>";
$page= "login.php";
    echo "<meta http-equiv=\"Refresh\" content=\"0;url=$page\">";
     exit();
    } 
    
  }

    function login($userid, $password){
		$userid = stripslashes($userid);
		$password = strtolower(stripslashes($password));
		$sql = sprintf("select *  from users WHERE userEmail=%s AND userPasword=%s ", $this->db->getSQLVS($userid, "text",'' , ''), $this->db->getSQLVS($password, "text",'' , ''));
		$results = $this->db->query($sql);
		$sqlaa = @mysql_fetch_assoc($results);
		//echo $sql;
		//exit;
			if(!$sqlaa) { $_SESSION['ewSessionMessage'] = "<div class='errormessage'><b>Invalid USERID/PASSWORD combination</b><br>Incorrect login detail or you dont access to this area..<br>If you have forgotten your password, click on the forgot password link below. </div>";
		header("location: login.php");
		exit;
			} else {

			$_SESSION["userId"] = $sqlaa["userId"];
			$_SESSION["userEmail"] = $sqlaa["userEmail"];
			$_SESSION['userFullname']=$sqlaa["userFullname"];

			$_SESSION['ewSessionMessage'] = "<div class='successmessage'><h1>Login Successful. Welcome ".$_SESSION['userFullname'].".</h1></div>";
            
			$page= "shortner.php";
			echo "<meta http-equiv=\"Refresh\" content=\"0;url=$page\">";
		exit;
			}
		return $e;
	}


	function logout(){

$_SESSION["userId"] = NULL;
$_SESSION["userEmail"] = NULL;
$_SESSION['userFullname'] = NULL;
unset($_SESSION['userId']);
unset($_SESSION['userEmail']);
unset($_SESSION['userFullname']);
 
 $_SESSION['ewSessionMessage'] = "<div class='successmessage'>You have successfully <b>logged out</b>.</div>";
		//header("location: login.php");
		$page= "index.php";
		echo "<meta http-equiv=\"Refresh\" content=\"0;url=$page\">";
	   exit();
	}


	function get_domains(){
        $sql = "select * from short_domains order by domainId desc";

        //echo $sql;
        //exit;
        $results = $this->db->query($sql);
        return $results;
    }

    function add_domains($domain){
        
			$sqls = sprintf ("insert into short_domains (domain)  Values (%s)",
		$this->db->getSQLVS($domain, "text",'',''));

        $querys = $this->db->query_nonselect($sqls);
//echo $querys;exit;

        if($querys > 0)
            {
                $_SESSION['ewSessionMessage'] = '<div class="successmessage">Your domain was successfully added.</div>';
           $page= "settings.php";
		echo "<meta http-equiv=\"Refresh\" content=\"0;url=$page\">";
                exit();
            }
            else {
                $_SESSION['ewSessionMessage'] = '<div class="errormessage">ERROR! Add domain failed, please try again.</div>';

           $page= "settings.php";
		echo "<meta http-equiv=\"Refresh\" content=\"0;url=$page\">";
                exit();
            }
    }

    function delete_domains($id){
        $sql = "DELETE from short_domains where domainId=".$id;

        //echo $sql;
        //exit;
        $querys = $this->db->query_nonselect($sql);

         if($querys > 0)
            {
                $_SESSION['ewSessionMessage'] = '<div class="successmessage">Your domain was successfully deleted.</div>';
           $page= "settings.php";
		echo "<meta http-equiv=\"Refresh\" content=\"0;url=$page\">";
                exit();
            }
            else {
                $_SESSION['ewSessionMessage'] = '<div class="errormessage">ERROR! delete domain failed, please try again.</div>';

           $page= "settings.php";
		echo "<meta http-equiv=\"Refresh\" content=\"0;url=$page\">";
                exit();
            }
       
    }

    function get_user_urls($userid){
        $sql = sprintf("SELECT * from short_urls WHERE userId = %s",$this->db->getSQLVS($userid, "int",'',''));
        //echo $sql;exit;
        $results = $this->db->query($sql);

        return $results;
    }

    function delete_user_urls($id){
        $sql = "DELETE from short_urls where urdId=".$id;

        //echo $sql;
        //exit;
        $querys = $this->db->query_nonselect($sql);

         if($querys > 0)
            {
                $_SESSION['ewSessionMessage'] = '<div class="successmessage">Your short domain was successfully deleted.</div>';
           $page= "urls.php";
		echo "<meta http-equiv=\"Refresh\" content=\"0;url=$page\">";
                exit();
            }
            else {
                $_SESSION['ewSessionMessage'] = '<div class="errormessage">ERROR! short domain deletion failed, please try again.</div>';

           $page= "urls.php";
		echo "<meta http-equiv=\"Refresh\" content=\"0;url=$page\">";
                exit();
            }
       
    }

     function update_shorturl($target_url,$userId,$urdId){
        $sql = "UPDATE short_urls SET long_url='".$target_url."' where urdId=".$urdId." and userId=".$userId;

        //echo $sql;
        //exit;
        $querys = $this->db->query_nonselect($sql);

         if($querys > 0)
            {
                $_SESSION['ewSessionMessage'] = '<div class="successmessage">Your short domain was successfully updated.</div>';
           $page= "urls.php";
		echo "<meta http-equiv=\"Refresh\" content=\"0;url=$page\">";
                exit();
            }
            else {
                $_SESSION['ewSessionMessage'] = '<div class="errormessage">ERROR! short domain update failed, please try again.</div>';

           $page= "urls.php";
		echo "<meta http-equiv=\"Refresh\" content=\"0;url=$page\">";
                exit();
            }
       
    }

    function view_shorturl($code){

        $sql = sprintf("SELECT long_url from short_urls WHERE short_code = %s",$this->db->getSQLVS($code, "text",'',''));
         //echo $sql;exit;
         $results = $this->db->query($sql);
   
        $b = $this->mail_get_assoc($results);
        $domain = $b['long_url'];

         if(!$b)
            {
           $page= "404.php";
                Header("HTTP/1.1 301 Moved Permanently");
                header("Location: ".$page."");
                exit();
            }
            else {
          Header("HTTP/1.1 301 Moved Permanently");
      header("Location: ".$domain."");
      exit();
            }
       
    }

    function generate_code(){
  $pw = '';
  $c  = 'bcdfghjklmnprstvwz'; //consonants except hard to speak ones
  $v  = 'aeiou';              //vowels
  $a  = $c.$v;                //both
 
  //use three syllables...
  FOR($i=0;$i < 2; $i++){
    $pw .= $c[RAND(0, STRLEN($c)-1)];
    $pw .= $v[RAND(0, STRLEN($v)-1)];
    //$pw .= $a[RAND(0, STRLEN($a)-1)];
  }
  //... and add a nice number
 $pw .= RAND(100,999);
 
  return $pw;
}
	
	



		function post_shorturl($target_url,$domain_name,$customcode){



			if($customcode == ""){
				$shortcode = $this->generate_code();
			} else {
				$chkcode =$this->check_code($customcode);
			//echo $chkcode.'xxxxx';exit;

			if ($chkcode == 1){
				$_SESSION['ewSessionMessage'] = '<div class="errormessage">Short code has already been used, please try again.</div>';
           		$page= "shortner.php";
				echo "<meta http-equiv=\"Refresh\" content=\"0;url=$page\">";
                exit();
			}
				$shortcode = $customcode;
			}

			$newdomain = "http://".$domain_name."/".$shortcode;
		
		
			$sqls = sprintf ("insert into short_urls (userId, long_url, short_url, short_code, used_domain)  Values (%s,%s,%s,%s,%s)", 
		$this->db->getSQLVS($_SESSION["userId"], "int",'',''),
		$this->db->getSQLVS($target_url, "text",'',''),
		$this->db->getSQLVS($newdomain, "text",'',''),
		$this->db->getSQLVS($shortcode, "text",'',''),
		$this->db->getSQLVS($domain_name, "text",'',''));

		//echo $sql;exit;

        $querys = $this->db->query_nonselect($sqls);
//echo $querys;exit;

        if($querys > 0)
            {
                $_SESSION['ewSessionMessage'] = '<div class="successmessage">Your short url was successfully created.</div>';
           $page= "shortner_success.php?url=".urlencode($newdomain);
		echo "<meta http-equiv=\"Refresh\" content=\"0;url=$page\">";
                exit();
            }
            else {
                $_SESSION['ewSessionMessage'] = '<div class="errormessage">ERROR! Shortner failed, please try again.</div>';

           $page= "shortner.php";
		echo "<meta http-equiv=\"Refresh\" content=\"0;url=$page\">";
                exit();
            }
           
		
		}


		 function check_code($code){
		$sql = sprintf("select short_code from short_urls WHERE short_code=%s", $this->db->getSQLVS($code, "text",'' , ''));
		$results = $this->db->query($sql);
		$sqlaa = @mysql_fetch_assoc($results);
		//echo $sqlaa;
		//exit;
			if(!$sqlaa) { return 0;
			} else {

			return 1;
			}
	}
	
	
	
function changePass() {
            $this->init->getConfig();
        
        @$p = strtolower($_POST["oldpass"]);
        @$p1 = strtolower($_POST["newpass1"]);
        @$p2 = strtolower($_POST["newpass2"]);
        $sql = "select dbo_customer_auth.cust_username, dbo_customer_auth.cust_password, dbo_tabcustomer.* from dbo_tabcustomer
left join dbo_customer_auth on dbo_tabcustomer.CustomerID = dbo_customer_auth.customerId WHERE dbo_customer_auth.cust_password='".$p."' AND dbo_tabcustomer.CustomerID=".$_SESSION["cust_no"];
        //echo $sql;exit;
        $results = $this->db->query($sql);
        $rs = @mysql_num_rows($results);
        if (!@$p or !@$p1 or !@$p2) { $e = "<div class='errormessage'>All fields are required</div>"; }
        elseif ($rs!=1) { $e ="<div class='errormessage'>Your old password was not entered correctly</div>"; }
        elseif (@$p1!=@$p2) { $e = "<div class='errormessage'>You entered 2 different new password</div>"; }
        else {
            $results = $this->db->query("UPDATE dbo_tabcustomer SET cust_password='".$p1."' WHERE cust_no=".$_SESSION["cust_no"]);
			$email = $rs["Email"];
			$msg = 'Your new password is '.$p1;
			$this->misc->sendHTMLemail($msg,$this->init->mailSender,$email,'Password Change',$this->init->companyName);
            $e = "<div class='successmessage'>Your password has been changed!</div>";
        }
        $_SESSION['SessionMessage'] = $e;
        return $e;
    }
	
	
		
		
		
	
	
	
	function mail_get_assoc($mail){
         return $this->db->get_assoc($mail);
    }
	function mail_num_rows($mail){
         return $this->db->num_rows($mail);
    }
	function mail_data_seek($mail,$int){
         return $this->db->data_seek($mail,$int);
    }

}


?>
