<?php
class dbaccess {

    var $error_msg;     // holds error messages
    var $host;          // Your MySQL host, may need to add port number as well
    var $user;          // The user name you will be using to connect. Should not be root
    var $pass;          // The password for $user
    var $db;            // The database you are connecting to
    var $conn;          // database link
    var $port;
    var $tCount;
    var $aSql;
    var $publickey;
    var $privatekey;
    var $antiBOT;
    var $ms;
    var $sitename;
    var $emailFrom;
    var $emailName;
    var $siteurl;
    var $timediff;
    var $cadpid;
    var $mertid;
    var $gtmertid;
    var $sitetitle;
    var $keywords;
    var $description;
    var $logo;
    var $mailsender;
    var $welcometext;
    var $greyboxurl;
    
    function dbaccess() {
        $this->host = 'localhost';
        //$this->host = '195.166.237.26';
        $this->user = 'root';
        $this->pass = '';
        //$this->pass = 'linksat12';
        $this->db   = 'shorturl';
        $this->port = '3306';
        $this->emailName = 'Site Administrator';
        $this->connect(true);
        $urlparts = explode('/',$_SERVER['REQUEST_URI']); 
        $urlhost = trim($_SERVER['HTTP_HOST']);
        $this->greyboxurl = 'http://'.$urlhost.'/'.trim($urlparts[1]).'/greybox/';
    }

    

    function connect($persist=true) {
        if( $persist )
            $this->conn = mysql_pconnect($this->host, $this->user, $this->pass);
        else
            $this->conn = mysql_connect($this->host, $this->user, $this->pass);
        if (!$this->conn) {
            $this->error_msg = mysql_error();
            return false;
        }
        if (!mysql_select_db($this->db)) {
            $this->error_msg = mysql_error();
            return false;
        }
        return true;
    }

    function disconnect() {
        mysql_close($this->conn);
    }
    function quote_smart($value) {
    // Stripslashes
        if (get_magic_quotes_gpc()) {
            $value = stripslashes($value);
        }
        // Quote if not a number or a numeric string
        if (!is_numeric($value)) {
            $value = "'" . mysql_real_escape_string($value) . "'";
        }
        return $value;
    }

    function query($sql) {
        $return = 0;
        $results = mysql_query($sql) or die(mysql_error());
        if (!$results) {
            return $return;
        }
        return $results;
    }

    function query_nonselect($sql) {
        $results = $this->query($sql) ;
        $value = mysql_affected_rows();
        return $value;
    }

    function num_rows($query) {
        return mysql_num_rows($query);
    }

    function get_array($query) {
        return mysql_fetch_array($query);
    }

    function insert_id() {
        $sql = "SELECT LAST_INSERT_ID()";
        $a = $this->query($sql);
        $b = $this->get_array($a);
        return $b[0];
    }

    function get_assoc($query) {
        return mysql_fetch_assoc($query);
    }

    function data_seek($dbquery,$int) {
        return mysql_data_seek($dbquery,$int);
    }

    function escape_string($string) {
        return mysql_real_escape_string($string);
    }

    function add_slashes($string) {
        if (get_magic_quotes_gpc()) {
            return $string;
        }else {
            return addslashes($string);
        }
    }

    function getSQLVS($theValue, $theType, $theDefinedValue, $theNotDefinedValue) {
        $theValue = get_magic_quotes_gpc() ? stripslashes($theValue) : $theValue;
        $theValue = function_exists("mysql_real_escape_string") ? mysql_real_escape_string($theValue) : mysql_escape_string($theValue);
        switch ($theType) {
            case "text":
                $theValue = ($theValue != "") ? "'" . $theValue . "'" : "NULL";
                break;
            case "long":
            case "int":
                $theValue = ($theValue != "") ? intval($theValue) : "NULL";
                break;
            case "double":
                $theValue = ($theValue != "") ? "'" . doubleval($theValue) . "'" : "NULL";
                break;
            case "date":
                $theValue = ($theValue != "") ? "'" . $theValue . "'" : "NULL";
                break;
            case "defined":
                $theValue = ($theValue != "") ? $theDefinedValue : $theNotDefinedValue;
                break;
        }
        return $theValue;
    }

    /** Add a sql statement **/
    function addSql($sql) {
        $this->tCount++;
        $this->aSql[$this->tCount] = $sql;
    }

    // Execute transactions
    function executeTransactSql() {
    // If start transaction ok
        if ($this->executeSQL("start transaction")) {
        // Executes a sql statements
            for ($i=1; $i<=count($this->aSql); $i++) {
                $res = $this->executeSQL($this->aSql[$i]);
                // Abort, if any error
                if (!$res) {
                    break;
                }
            }
            // If all statements executes ok, commit transaction else rollback.
            if ($res) {
                $res = $this->executeSQL("commit");
                return true;
            }
            else {
                $res = $this->executeSQL("rollback");
                return false;
            }
        }
    }

    // Executes a sql statement in  MySQL database
    function executeSQL($sql) {
        if(empty($sql))
            $res = 0; // Error in connection or SQL clausule.
        if (!($res = $this->query($sql))) {
            $res = 0; //Error occurred
        }
        return $res;
    }

}
                        ?>