<?php 
if (!isset($_SESSION)) {
  session_start();
}
//ini_set('display_errors', 1);

header("Expires: Mon, 26 Jul 1997 05:00:00 GMT"); // Date in the past
header("Last-Modified: " . gmdate("D, d M Y H:i:s") . " GMT"); // Always modified
header("Cache-Control: private, no-store, no-cache, must-revalidate"); // HTTP/1.1 
header("Cache-Control: post-check=0, pre-check=0", false);
header("Pragma: no-cache"); // HTTP/1.0

  ob_start();
  //LoadUserLevel();
  if ($_SESSION["userId"] != "") {
  ob_end_clean();
  header("Location: shortner.php");
  exit();
} 

require_once("classes/function.php"); 
$general = new general();


if(@$_POST['kgct']){ 
  //echo $_REQUEST['userid'];
//  exit;
// require_once("login_auth.php");  
// $auth = new auth(); 
$e = $general->login($_REQUEST['userid'], $_REQUEST['userpassword']); 
}
?>
<!DOCTYPE html>
<html lang="en">
<head>

  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta name="description" content="">
  <meta name="author" content="">

  <title>URLShortner - Sign In</title>

  <!-- Bootstrap core CSS -->
  <link href="vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">
  <link href="css/signin.css" rel="stylesheet">
<link href="css/icon.css" rel="stylesheet">
</head>

<body class="text-center">
 
    <form class="form-signin" action="login.php" method="POST" name="Login" id="Login">
      <input type="Hidden" name="kgct" value="login">
      <h1>URL Shortener</h1>
      <h2 class="h3 mb-3 font-weight-normal">Please sign in</h2>
       <?php require_once('error_msg.php'); ?>
      <label for="userid" class="sr-only">Email address</label>
      <input type="email" id="userid" name="userid" class="form-control" placeholder="Email address" required autofocus>
      <label for="userpassword" class="sr-only">Password</label>
      <input type="password" id="userpassword" name="userpassword" class="form-control" placeholder="Password" required>
      
      <button class="btn btn-lg btn-primary btn-block" type="submit">Sign in</button>
      <p class="mt-5 mb-3 text-muted">&copy; 2017-2018</p>
    </form>
  </body>

</html>
