<?php 
if (!isset($_SESSION)) {
  session_start();
}
//ini_set('display_errors', 1);
require_once("classes/function.php"); 
$general = new general();

$general->checkLogin();

if(@$_POST['loji22']){ 
$general->add_domains($_REQUEST['domain']); 
}

if($_GET['action']=='del'){ 
$general->delete_domains($_GET['id']); 
}

$mainNav = 3;
?>
<!DOCTYPE html>
<html lang="en">

<head>

  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta name="description" content="">
  <meta name="author" content="">

  <title>URLShortner - URLs</title>

  <!-- Bootstrap core CSS -->
  <link href="vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">
  <link href="https://cdn.datatables.net/responsive/2.2.3/css/responsive.bootstrap4.min.css" rel="stylesheet">
   <link href="https://cdn.datatables.net/1.10.19/css/dataTables.bootstrap4.min.css" rel="stylesheet">
 <link href="css/icon.css" rel="stylesheet">
</head>

<body>

 <?php require_once("header.php"); ?>

  <!-- Page Content -->
  <div class="container">
    
    <div class="row">
      
      <div class="col-lg-12">
        <h1 class="mt-5">Host Domains</h1>
        <p class="lead">list of hosted domains used for short urls.</p>
        <hr>
         <a href="javascript:void(0);" data-title="Add" data-toggle="modal" data-target="#add" class="btn btn-primary"><i class="fas fa-plus"></i> Add Domain</a>
         <br><br>
        
        <table id="example" class="table table-striped table-bordered" style="width:100%">
    <thead>
        <tr>
            <th>Domain Name</th>
            <th>Action(s)</th>
        </tr>
    </thead>
    <tbody>
      <?php 
                  $mydomains = $general->get_domains();
                  while ($row_domain= $general->mail_get_assoc($mydomains)) {?>
        <tr>
            <td><?php echo $row_domain['domain']; ?></td>
            <td><a href="settings.php?action=del&id=<?php echo $row_domain['domainId']; ?>" ><span class="glyphicon glyphicon-trash"></span></a></td>
        </tr>
       <?php } ?>
        <tbody>
</table>


      </div>
    
    </div>

    <?php require_once("footer.php"); ?>
  </div>

  <div class="modal fade" id="add" tabindex="-1" role="dialog" aria-labelledby="edit" aria-hidden="true">
      <div class="modal-dialog">
    <div class="modal-content">
      <form action="settings.php" method="POST" name="generate" id="generate">
      <input type="Hidden" name="loji22" value="login">
          <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true"><span class="glyphicon glyphicon-remove" aria-hidden="true"></span></button>
        <h4 class="modal-title custom_align" id="Heading">Add Domain</h4>
      </div>
          <div class="modal-body">
          <div class="form-group">
        <input class="form-control" type="text" id="domain" name="domain" placeholder="Enter Domain" required>
        </div>
      </div>
          <div class="modal-footer ">
        <button type="submit" class="btn btn-warning btn-lg" style="width: 100%;"><span class="glyphicon glyphicon-ok-sign"></span> Add</button>
      </div>
    </form>
        </div>
    <!-- /.modal-content --> 
  </div>
      <!-- /.modal-dialog --> 
    </div>
   
    
    

  <!-- Bootstrap core JavaScript -->
  <script src="vendor/jquery/jquery.min.js"></script>
  <script src="vendor/bootstrap/js/bootstrap.bundle.min.js"></script>
  <script src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js"></script>
  <script src="https://cdn.datatables.net/1.10.19/js/dataTables.bootstrap4.min.js"></script>
  <script src="https://cdn.datatables.net/responsive/2.2.3/js/dataTables.responsive.min.js"></script>
  <script src="https://cdn.datatables.net/responsive/2.2.3/js/responsive.bootstrap4.min.js"></script>
<script>
$(document).ready(function() {
    $('#example').DataTable();
} );
</script>
</body>

</html>
