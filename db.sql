/*
SQLyog Community v12.5.0 (64 bit)
MySQL - 10.1.31-MariaDB : Database - shorturl
*********************************************************************
*/

/*!40101 SET NAMES utf8 */;

/*!40101 SET SQL_MODE=''*/;

/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;
/*Table structure for table `admin` */

DROP TABLE IF EXISTS `admin`;

CREATE TABLE `admin` (
  `adminId` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(100) DEFAULT NULL,
  `password` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`adminId`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `admin` */

/*Table structure for table `short_domains` */

DROP TABLE IF EXISTS `short_domains`;

CREATE TABLE `short_domains` (
  `domainId` int(11) NOT NULL AUTO_INCREMENT,
  `domain` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`domainId`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

/*Data for the table `short_domains` */

insert  into `short_domains`(`domainId`,`domain`) values 
(1,'goth.ly'),
(2,'xpat.ng');

/*Table structure for table `short_urls` */

DROP TABLE IF EXISTS `short_urls`;

CREATE TABLE `short_urls` (
  `urdId` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `userId` int(11) DEFAULT '0',
  `used_domain` varchar(255) DEFAULT NULL,
  `long_url` varchar(255) NOT NULL,
  `short_url` varchar(255) DEFAULT NULL,
  `short_code` varbinary(6) DEFAULT NULL,
  `date_created` datetime DEFAULT CURRENT_TIMESTAMP,
  `counter` int(10) DEFAULT '0',
  PRIMARY KEY (`urdId`),
  KEY `short_code` (`short_code`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;

/*Data for the table `short_urls` */

insert  into `short_urls`(`urdId`,`userId`,`used_domain`,`long_url`,`short_url`,`short_code`,`date_created`,`counter`) values 
(1,1,'goth.ly','https://docs.growlearnteach.com/knowledgebase/how-to-configure-amazon-s3-to-stream-your-videos/','http://goth.ly/jano144','jano14','2019-03-29 21:42:19',0),
(2,1,'goth.ly','https://datatables.net/extensions/responsive/examples/styling/bootstrap4.html','http://goth.ly/loji225','loji22','2019-03-29 21:42:51',0),
(4,1,'goth.ly','https://datatables.net/extensions/responsive/examples/styling/bootstrap4.html','http://goth.ly/cohi177','cohi17','2019-03-29 21:45:00',0);

/*Table structure for table `users` */

DROP TABLE IF EXISTS `users`;

CREATE TABLE `users` (
  `userId` int(11) NOT NULL AUTO_INCREMENT,
  `userFullname` varchar(255) DEFAULT NULL,
  `userEmail` varchar(255) DEFAULT NULL,
  `userPasword` varchar(255) DEFAULT NULL,
  `userDateregistered` datetime DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`userId`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

/*Data for the table `users` */

insert  into `users`(`userId`,`userFullname`,`userEmail`,`userPasword`,`userDateregistered`) values 
(1,'Demo User','demo@demo.com','123456','2019-03-29 19:59:18');

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
