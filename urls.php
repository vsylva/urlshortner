<?php 
if (!isset($_SESSION)) {
  session_start();
}
//ini_set('display_errors', 1);
require_once("classes/function.php"); 
$general = new general();
$general->checkLogin();

if($_GET['action']=='delete'){ 
$general->delete_user_urls($_GET['id']); 
}

if(@$_POST['loji234']){ 
$general->update_shorturl($_REQUEST['target_url'], $_REQUEST['userId'], $_REQUEST['urdId']); 
}

$mainNav = 2;
?>
<!DOCTYPE html>
<html lang="en">

<head>

  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta name="description" content="">
  <meta name="author" content="">

  <title>URLShortner - URLs</title>

  <!-- Bootstrap core CSS -->
  <link href="vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">
  <link href="https://cdn.datatables.net/responsive/2.2.3/css/responsive.bootstrap4.min.css" rel="stylesheet">
   <link href="https://cdn.datatables.net/1.10.19/css/dataTables.bootstrap4.min.css" rel="stylesheet">
 <link href="css/icon.css" rel="stylesheet">
 <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.8.1/css/all.css" integrity="sha384-50oBUHEmvpQ+1lW4y57PTFmhCaXp0ML5d60M1M7uH2+nqUivzIebhndOJK28anvf" crossorigin="anonymous">
</head>

<body>

 <?php require_once("header.php"); ?>

  <!-- Page Content -->
  <div class="container">
    
    <div class="row">
      
      <div class="col-lg-12">
        <h1 class="mt-5">Search your short history</h1>
        <p class="lead">Your entire URL shortening activity is right here.</p>
        <hr>
        <h2>History</h2><br><br>
        
        <table id="example" class="table table-striped table-bordered" style="width:100%">
    <thead>
        <tr>
            <th>Short link</th>
            <th>Title / Long URL</th>
            <th>Action(s)</th>
        </tr>
    </thead>
    <tbody>
      <?php 
                  $mydomains = $general->get_user_urls($_SESSION["userId"]);
                  while ($row_domain= $general->mail_get_assoc($mydomains)) {?>
        <tr>
            <td><?php echo $row_domain['short_url']; ?></td>
            <td><?php echo $row_domain['long_url']; ?></td>
            <td><a href="javascript:void(0);" data-title="Edit" data-toggle="modal" data-target="#edit<?php echo $row_domain['urdId']; ?>"><span class="glyphicon glyphicon-pencil"></span></a>  <a href="urls.php?action=delete&id=<?php echo $row_domain['urdId']; ?>" ><span class="glyphicon glyphicon-trash"></span></a>  <a href="https://www.facebook.com/sharer/sharer.php?u=<?php echo $row_domain['short_url']; ?>" target="_blank" ><i class="fab fa-facebook-square"></i></a>  <a href="https://twitter.com/intent/tweet?text=<?php echo $row_domain['short_url']; ?>" target="_blank" ><i class="fab fa-twitter-square"></i></a></td>
        </tr>
       <?php } ?>
        <tbody>
</table>


      </div>
    
    </div>

    <?php require_once("footer.php"); ?>
  </div>

<?php 
                  $editdomains = $general->get_user_urls($_SESSION["userId"]);
                  while ($row_domains= $general->mail_get_assoc($editdomains)) {?>
  <div class="modal fade" id="edit<?php echo $row_domains['urdId']; ?>" tabindex="-1" role="dialog" aria-labelledby="edit" aria-hidden="true">
      <div class="modal-dialog">
    <div class="modal-content">
      <form action="urls.php" method="POST" name="generate" id="generate">
      <input type="Hidden" name="loji234" value="login">
          <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true"><span class="glyphicon glyphicon-remove" aria-hidden="true"></span></button>
        <h4 class="modal-title custom_align" id="Heading">Edit Your URL</h4>
      </div>
          <div class="modal-body">
          <div class="form-group">
        <input class="form-control" type="url" id="target_url" name="target_url" value="<?php echo $row_domains['long_url']; ?>">
        </div>
        <div class="form-group">
        <input type="Hidden" name="userId" value="<?php echo $row_domains['userId']; ?>">
        <input type="Hidden" name="urdId" value="<?php echo $row_domains['urdId']; ?>">
        <input class="form-control" type="text" readonly value="<?php echo $row_domains['short_url']; ?>">
        </div>
      </div>
          <div class="modal-footer ">
        <button type="submit" class="btn btn-warning btn-lg" style="width: 100%;"><span class="glyphicon glyphicon-ok-sign"></span> Update</button>
      </div>
    </form>
        </div>
    <!-- /.modal-content --> 
  </div>
      <!-- /.modal-dialog --> 
    </div>
    <?php } ?>
    
 
  <!-- Bootstrap core JavaScript -->
  <script src="vendor/jquery/jquery.min.js"></script>
  <script src="vendor/bootstrap/js/bootstrap.bundle.min.js"></script>
  <script src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js"></script>
  <script src="https://cdn.datatables.net/1.10.19/js/dataTables.bootstrap4.min.js"></script>
  <script src="https://cdn.datatables.net/responsive/2.2.3/js/dataTables.responsive.min.js"></script>
  <script src="https://cdn.datatables.net/responsive/2.2.3/js/responsive.bootstrap4.min.js"></script>
<script>
$(document).ready(function() {
    $('#example').DataTable();
} );
</script>
</body>

</html>
