 <!-- Navigation -->
  <nav class="navbar navbar-expand-lg navbar-dark bg-dark static-top">
    <div class="container">
      <a class="navbar-brand" href="#">Shortener</a>
      <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarResponsive" aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
      </button>
      <div class="collapse navbar-collapse" id="navbarResponsive">
       <?php if (isset($_SESSION["userId"])) { ?> 
        <ul class="navbar-nav ml-auto">
          <li class="nav-item <?php if ($mainNav == 1){ ?>active<?php }?> ">
            <a class="nav-link" href="shortner.php">Shortner
              <span class="sr-only">(current)</span>
            </a>
          </li>
          <li class="nav-item <?php if ($mainNav == 2){ ?>active<?php }?> ">
            <a class="nav-link" href="urls.php">URLs</a>
          </li>
          <li class="nav-item <?php if ($mainNav == 3){ ?>active<?php }?> ">
            <a class="nav-link" href="settings.php">Settings</a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="logout.php">Log Out</a>
          </li>
        </ul>
      <?php } ?> 
      </div>
    </div>
  </nav>

  <?php require_once('error_msg.php'); ?>