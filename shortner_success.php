<?php 
if (!isset($_SESSION)) {
  session_start();
}

require_once("classes/function.php"); 
$general = new general();
$general->checkLogin();
//ini_set('display_errors', 1);
$mainNav = 1;
?>
<!DOCTYPE html>
<html lang="en">
<head>

  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta name="description" content="">
  <meta name="author" content="">

  <title>URLShortner - Start Bootstrap Template</title>

  <!-- Bootstrap core CSS -->
  <link href="vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">
<link href="css/icon.css" rel="stylesheet">
<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.8.1/css/all.css" integrity="sha384-50oBUHEmvpQ+1lW4y57PTFmhCaXp0ML5d60M1M7uH2+nqUivzIebhndOJK28anvf" crossorigin="anonymous">
</head>

<body>

 <?php require_once("header.php"); ?>

  <!-- Page Content -->
  <div class="container">
    
    <div class="row">
      
      <div class="col-lg-12">
        <h1 class="mt-5">Go Short</h1>
        <p class="lead">Turn obscenely long URLs into the tidiest links on the Web.</p>
        <hr>
        <form  class="needs-validation" action="index.php" novalidate>
  <div class="form-group">
    <label for="target_url">Short link</label>
    <div style="font-family:Arial, Helvetica, sans-serif; font-size:18px; font-weight:bold; background-color:#D7D7D7; color:#000; border:solid #999 1px; padding: 10px; width:530px"> <?php echo $_REQUEST['url'];?></div>
  </div>
  <div class="form-group">
         <label for="customcode">Share your short link</label>
         <div id="share_links">
                        <a href="https://www.facebook.com/sharer/sharer.php?u=<?php echo $_REQUEST['url'];?>" class="btn btn-primary" id="fb_share_btn" target="_blank" tabindex="-1"><i class="fab fa-facebook-square"></i> Facebook</a>

                        <a href="https://twitter.com/intent/tweet?text=<?php echo $_REQUEST['url'];?>" class="btn btn-primary" id="tw_share_btn" target="_blank" tabindex="-1"><i class="fab fa-twitter-square"></i> Twitter</a> 
                    </div>
      </div>
<br><br>


  <a href="shortner.php" class="btn btn-primary"><i class="fas fa-chevron-left"></i> Shorten Another</a>
</form>

      </div>
    
    </div>

    <?php require_once("footer.php"); ?>
  </div>

  

  <!-- Bootstrap core JavaScript -->
  <script src="vendor/jquery/jquery.min.js"></script>
  <script src="vendor/bootstrap/js/bootstrap.bundle.min.js"></script>
<script>
// Example starter JavaScript for disabling form submissions if there are invalid fields
(function() {
  'use strict';
  window.addEventListener('load', function() {
    // Fetch all the forms we want to apply custom Bootstrap validation styles to
    var forms = document.getElementsByClassName('needs-validation');
    // Loop over them and prevent submission
    var validation = Array.prototype.filter.call(forms, function(form) {
      form.addEventListener('submit', function(event) {
        if (form.checkValidity() === false) {
          event.preventDefault();
          event.stopPropagation();
        }
        form.classList.add('was-validated');
      }, false);
    });
  }, false);
})();
</script>
</body>

</html>
