<?php 
if (!isset($_SESSION)) {
  session_start();
}
//ini_set('display_errors', 1);header("Expires: Mon, 26 Jul 1997 05:00:00 GMT"); // Date in the past
header("Last-Modified: " . gmdate("D, d M Y H:i:s") . " GMT"); // Always modified
header("Cache-Control: private, no-store, no-cache, must-revalidate"); // HTTP/1.1 
header("Cache-Control: post-check=0, pre-check=0", false);
header("Pragma: no-cache"); // HTTP/1.0

  ob_start();
  //LoadUserLevel();
  if ($_SESSION["userId"] != "") {
  ob_end_clean();
  header("Location: shortner.php");
  exit();
} else {
  ob_end_clean();
  header("Location: login.php");
  exit();
}
?>
<!DOCTYPE html>
<html lang="en">

<head>

  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta name="description" content="">
  <meta name="author" content="">

  <title>URLShortner - Home</title>

  <!-- Bootstrap core CSS -->
  <link href="vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">
<link href="css/icon.css" rel="stylesheet">
</head>

<body>

 <?php require_once("header.php"); ?>

  <!-- Page Content -->
  <div class="container">
    
    <div class="row">
      
      <div class="col-lg-12">
        
        <div class="card" style="margin-top: 30px">
            <div class="card-body">
                     <div class="row">
                <div class="col-sm-6">
                  
                    <h1>Shortener</h1>
                     <p class="lead">Go Short</p><br><br>
                    <p>The URL Shortener creates short links for links on the web.</p>
                    <p>Please sign in to use the service</p>
                    <a href="login.php" class="btn btn-primary">Sign In</a>
                </div>
                                 <div class="col-sm-6">
                                    <img class="card-img-top" src="images/preview-xl.jpg" alt="Card image cap">
                 </div>
               </div>
        
            </div>
        </div>

      </div>
    
    </div>

    <?php require_once("footer.php"); ?>
  </div>

  

  <!-- Bootstrap core JavaScript -->
  <script src="vendor/jquery/jquery.min.js"></script>
  <script src="vendor/bootstrap/js/bootstrap.bundle.min.js"></script>
</body>

</html>
