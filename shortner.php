<?php 
if (!isset($_SESSION)) {
  session_start();
}
//ini_set('display_errors', 1);
require_once("classes/function.php"); 
$general = new general();

$general->checkLogin();

if(@$_POST['cati730']){ 
$general->post_shorturl($_REQUEST['target_url'], $_REQUEST['domain_name'], $_REQUEST['customcode']); 
}

$mainNav = 1;
?>
<!DOCTYPE html>
<html lang="en">

<head>

  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta name="description" content="">
  <meta name="author" content="">

  <title>URLShortner - Start Bootstrap Template</title>

  <!-- Bootstrap core CSS -->
  <link href="vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">
<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.8.1/css/all.css" integrity="sha384-50oBUHEmvpQ+1lW4y57PTFmhCaXp0ML5d60M1M7uH2+nqUivzIebhndOJK28anvf" crossorigin="anonymous">
<link href="css/icon.css" rel="stylesheet">
</head>

<body>

 <?php require_once("header.php"); ?>

  <!-- Page Content -->
  <div class="container">
    
    <div class="row">
      
      <div class="col-lg-12">
        <h1 class="mt-5">Go Short</h1>
        <p class="lead">Turn obscenely long URLs into the tidiest links on the Web.</p>
        <hr>
          <form class="needs-validation" action="shortner.php" method="POST" name="generate" id="generate" novalidate>
      <input type="Hidden" name="cati730" value="login">
  <div class="form-group">
    <label for="target_url">Enter long URL:</label>
    <input type="url" class="form-control" id="target_url" name="target_url" placeholder="http://" required>
        <div class="invalid-feedback">
          Enter long URL
        </div>
  </div>
  <div class="row">
    <div class="col">
      <div class="form-group">
         <label for="domain_name">Choose domain:</label>
          <select id="domain_name" name="domain_name" class="form-control" required>
                <?php 
                  $domains = $general->get_domains();
                  while ($row_domain= $general->mail_get_assoc($domains)) {?>
                  <option  value="<?php echo $row_domain['domain']; ?>"><?php echo $row_domain['domain']; ?></option>
                  <?php } ?>
      </select>
        <div class="invalid-feedback">
          Choose a domain
        </div>
      </div>
    </div>
    <div class="col">
      <div class="form-group">
         <label for="customcode">Enter custom link:</label>
          <input id="customcode" name="customcode" type="text" class="form-control" placeholder="(Optional)">
      </div>
    </div>
  </div>

  <button type="submit" class="btn btn-primary">Shorten <i class="fas fa-chevron-right"></i></button>
</form>

      </div>
    
    </div>

    <?php require_once("footer.php"); ?>
  </div>

  

  <!-- Bootstrap core JavaScript -->
  <script src="vendor/jquery/jquery.min.js"></script>
  <script src="vendor/bootstrap/js/bootstrap.bundle.min.js"></script>
<script>
// Example starter JavaScript for disabling form submissions if there are invalid fields
(function() {
  'use strict';
  window.addEventListener('load', function() {
    // Fetch all the forms we want to apply custom Bootstrap validation styles to
    var forms = document.getElementsByClassName('needs-validation');
    // Loop over them and prevent submission
    var validation = Array.prototype.filter.call(forms, function(form) {
      form.addEventListener('submit', function(event) {
        if (form.checkValidity() === false) {
          event.preventDefault();
          event.stopPropagation();
        }
        form.classList.add('was-validated');
      }, false);
    });
  }, false);
})();
</script>
</body>

</html>
